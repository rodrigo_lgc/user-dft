### A NodeJS, MongoDB + cloud, Express, React + Redux solution for Dafiti front end test, an user account site.###

### UNDER DEVELOPMENT ###
* [ ] Home
* [ ] Order List CSS
* [ ] Wishlist

### DONE ###
#### [x] Site Structure ####
* React, MongoDB, Webback, Express
* Basic FED, BED

#### [x] Profile ####
* Layout
* Store
* Read, Update DB
* Form Validation

#### [x] Address List ####
* Layout
* Simple card selection 

#### [x] Order List ####
* HTMl layout

### HOW TO USE IT ###
* npm install
* npm run dev
* [localhost](http://localhost:8080)


### STRUCTURE ###

```bash
│   .babelrc
│   package.json
│   postcss.config.js
│   sass-loader.js
├───client
│   │   index.js
│   │   reducers.js
│   │   routes.js
│   │   
│   ├───App
│   │       index.js
│   │       layout.js
│   │       routers.js
│   │       store.js
│   │       
│   ├───component
│   │   ├───Breadcrumbs
│   │   │       index.js
│   │   │       
│   │   ├───Card
│   │   │       index.js
│   │   │       
│   │   ├───Footer
│   │   │       index.js
│   │   │       
│   │   ├───Form
│   │   │       form.js
│   │   │       input.js
│   │   │       select.js
│   │   │       
│   │   ├───Header
│   │   │       index.js
│   │   │       
│   │   ├───Loader
│   │   │       index.js
│   │   │       
│   │   ├───Overlay
│   │   │       index.js
│   │   │       
│   │   ├───Page
│   │   │       index.js
│   │   │       
│   │   ├───Section
│   │   │       index.js
│   │   │       
│   │   └───Sidebar
│   │           index.js
│   │           
│   ├───container
│   │   ├───AddressList
│   │   │       index.js
│   │   │       
│   │   ├───Home
│   │   │       index.js
│   │   │       
│   │   ├───Main
│   │   │       index.js
│   │   │       
│   │   ├───NotFound
│   │   │       index.js
│   │   │       
│   │   ├───OrderHistory
│   │   │       index.js
│   │   │       
│   │   ├───Profile
│   │   │       index.js
│   │   │       
│   │   └───Wishlist
│   │           index.js
│   │           
│   ├───ducks
│   │       profile.js
│   │       
│   ├───scss
│   │       main.scss
│   │       normalize.css
│   │       _animations.scss
│   │       _breadcrumbs.scss
│   │       _card.scss
│   │       _form.scss
│   │       _icons.scss
│   │       _layout.scss
│   │       _loader.scss
│   │       _typography.scss
│   │       _variables.scss
│   │       
│   └───utils
├───config
│       config.js
│       webpack.config.devClient.js
│       webpack.config.devServer.js
│       
├───public
│   │   favicon.ico
│   │   
│   └───images
│       │   brand-dafiti.png
│       │   
│       └───icons
│               icon-arrow-right.svg
│               
└───server
    │   app.js
    │   index.js
    │   populate.js
    │   
    ├───controllers
    │       ProfileController.js
    │       
    ├───models
    │       Profile.js
    │       
    ├───routes
    │   │   index.js
    │   │   
    │   └───api
    │           profile.js
    │           
    └───source
            index.js
```

const express = require('express');
const router = express.Router();
const profile = require('./api/profile.js');

router.use('/profile', profile);

export default router
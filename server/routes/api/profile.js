const express = require('express')
const router = express.Router()
const profileController = require('../../controllers/ProfileController')

router.get('/', function (req, res, next) {
	
	profileController.find({}, function (err, results) {
		if (err) {
			console.log(err);
			res.json({
				success: 0,
				error: err
			});
			return;
		}
		res.json({
			success: 1,
			data: results
		});
	});
})

router.put('/:id', function (req, res, next) {
	const id = req.params.id
	const data = req.body
	console.log('data dentro', req.body);
	

	profileController.findOneAndUpdate(id, data, function (err, result) {

		if (err) {
			console.log(err);
			res.status(500).json({
				success: 0,
				data: result
			});
			return;
		}

		res.status(200).json({
			success: 1,
			data: result
		});
	});
})

/*router.post('/', function (req, res, next) {
	profileController.create(req.body, function (err, result) {
		if (err) {
			console.log(err);
			res.json({
				success: 0,
				error: err
			})
			return;
		}

		res.json({
			success: 1,
			data: result
		});
	});
});*/

module.exports = router
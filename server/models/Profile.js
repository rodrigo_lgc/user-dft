const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    fullName: {
        type: String,
        trim: true,
        maxlength: 255
    },
    sex: {
        type: String,
        enum: ['M', 'F', 'Other']
    },
    age: {
        type: Number,
        max: 150
    },
    email: {
        type: String
    }

});

module.exports = mongoose.model('Profile', ProfileSchema);

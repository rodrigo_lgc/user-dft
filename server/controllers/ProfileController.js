const Profile = require('../models/Profile')

module.exports = {
	create: function (params, callback) {
		Profile.create(params, function (err, result) {
			if (err) {
				callback(err, null);
				return
			}
			callback(null, result);
		});
	},

	find: function (params, callback) {		
		Profile.find(params, '_id fullName age sex email', function (err, results) {
			if (err) {
				callback(err, null);
				return;
			}
			callback(null, results);
		})
	},

	findOneAndUpdate: function (id, data, callback) {		
		Profile.findOneAndUpdate({'_id': id}, {$set: data } ,function (err, results) {
			if (err) {
				callback(err, null);
				return;
			}
			callback(null, results);
		})
	}
}
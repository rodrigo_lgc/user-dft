import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import Page from '../../component/Page';
import Header from '../../component/Header';
import Breadcrumbs from '../../component/Breadcrumbs';
import Section from '../../component/Section';
import Sidebar from '../../component/Sidebar';
import Footer from '../../component/Footer';
import Loader from '../../component/Loader';

import { config } from './../../../config/config';

export default class Wishlist extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showLoader: false,
            loaderMessages: undefined
        }
    }

    render() {

        return (
            <Page pageClass={'wishlist'}>
                <Helmet>
                    <meta name="description" content="Wishlist" />
                </Helmet>
                <Header />
                <Breadcrumbs items={[{ title: 'Home', link: "/", position: 1 },{ title: 'Wishlist', link: "/wishlist", position: 2 }]} />
                <main className='main'>
                    <div className="container">              
                        <Sidebar />
                        <Section sectionClass="wishlist" title="Wishlist" />
                   </div>
                </main>
                <Loader show={this.state.showLoader} > {this.state.loaderMessages} </Loader>
                <Footer />
            </Page>
        );
    }
}
import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import Page from '../../component/Page';
import Header from '../../component/Header';
import Sidebar from '../../component/Sidebar';
import Section from '../../component/Section';
import Footer from '../../component/Footer';
import Loader from '../../component/Loader';

import { config } from './../../../config/config';

export default class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showLoader: false,
            loaderMessages: undefined
        }
    }

    render() {

        return (
            <Page pageClass={'home'}>
                <Helmet>
                    <meta name="description" content="My Account" />
                </Helmet>
                <Header />
                <main className='main'>
                    <div className="container">              
                        <Sidebar />
                        <Section sectionClass="home" title="My Account" />
                   </div>
                </main>
                <Loader show={this.state.showLoader} > {this.state.loaderMessages} </Loader>
                <Footer />
            </Page>
        );
    }
}
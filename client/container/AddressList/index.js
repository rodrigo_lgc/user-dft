import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Store from 'store';

import Page from '../../component/Page';
import {AddressCard} from '../../component/Card';
import Header from '../../component/Header';
import Breadcrumbs from '../../component/Breadcrumbs';
import Section from '../../component/Section';
import Sidebar from '../../component/Sidebar';
import Footer from '../../component/Footer';
import Loader from '../../component/Loader';

import { config } from './../../../config/config';

//import { fetchAddress } from '../../ducks/address';

export default class AddressList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showLoader: false,
            loaderMessages: undefined,
            selected: 1
        }
        this.handleClick = this.handleClick.bind(this)
    }
    
    handleClick(id) {
        this.setState({ selected: id})    
        console.log('id',id);
        
    }

    render() {

        return (
            <Page pageClass={'address-list'}>
                <Helmet>
                    <meta name="description" content="Address List" />
                </Helmet>
                <Header />
                <Breadcrumbs items={[{ title: 'Home', link: "/", position: 1 },{ title: 'Address List', link: "/address-list", position: 2 }]} />
                <main className='main'>
                    <div className="container">              
                        <Sidebar />
                        <Section sectionClass="address-list" title="Address List">
                            <div className="content">
                                <AddressCard selected={this.state.selected === 0} id={0} mainOnClick={(id) => this.handleClick(id)} />
                                <AddressCard selected={this.state.selected == 1} id={1} mainOnClick={(id) => this.handleClick(id)} />
                                <AddressCard selected={this.state.selected == 2} id={2} mainOnClick={(id) => this.handleClick(id)} />
                                <AddressCard selected={this.state.selected == 3} id={3} mainOnClick={(id) => this.handleClick(id)} />
                                <AddressCard selected={this.state.selected == 4} id={4} mainOnClick={(id) => this.handleClick(id)} />
                            </div>
                        </Section>
                   </div>
                </main>
                <Loader show={this.state.showLoader} > {this.state.loaderMessages} </Loader>
                <Footer />
            </Page>
        );
    }
}
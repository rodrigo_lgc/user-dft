import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import Page from '../../component/Page';
import { OrderCard, OrderItem } from '../../component/Card';
import Header from '../../component/Header';
import Breadcrumbs from '../../component/Breadcrumbs';
import Section from '../../component/Section';
import Sidebar from '../../component/Sidebar';
import Footer from '../../component/Footer';
import Loader from '../../component/Loader';

import { config } from './../../../config/config';

export default class OrderHistory extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showLoader: false,
            loaderMessages: undefined
        }
    }

    render() {

        return (
            <Page pageClass={'order-history'}>
                <Helmet>
                    <meta name="description" content="Order History" />
                </Helmet>
                <Header />
                <Breadcrumbs items={[{ title: 'Home', link: "/", position: 1 },{ title: 'Order History', link: "/order-history", position: 2 }]} />
                <main className='main'>
                    <div className="container">              
                        <Sidebar />
                        <Section sectionClass="order-history" title="Order History">
                            <OrderCard id={0}>
                                <OrderItem />
                                <OrderItem />
                                <OrderItem />
                            </OrderCard>
                        </Section>
                   </div>
                </main>
                <Loader show={this.state.showLoader} > {this.state.loaderMessages} </Loader>
                <Footer />
            </Page>
        );
    }
}
import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import Store from 'store';

import Page from '../../component/Page';
import Header from '../../component/Header';
import Breadcrumbs from '../../component/Breadcrumbs';
import Section from '../../component/Section';
import Sidebar from '../../component/Sidebar';
import Footer from '../../component/Footer';
import Loader from '../../component/Loader';
import Form from '../../component/Form/form';
import Input from '../../component/Form/input';
import Select from '../../component/Form/select';

import { config } from './../../../config/config';

import { fetchProfile, updateProfile  } from '../../ducks/profile';
class Profile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            _id: '',
            showLoader: false,
            loaderMessages: undefined,
            showFormErrors: true,
            newData: false,
            changedData: false,
            checkFiles: false,
            age: '',
            ageError: true,
            sex: '',
            sexOptions: ['M', 'F', 'Other'] ,
            sexError: true,
            fullName: '',
            fullNameError: true,
            email: '',
            emailError: true,
        }
        this.handleValuesChange = this.handleValuesChange.bind(this);
        this.formValidation = this.formValidation.bind(this);
        this.handleProfileUpdate = this.handleProfileUpdate.bind(this);
        this.handleFetchProfile = this.handleFetchProfile.bind(this);
    }

    componentDidMount() {
        this.handleFetchProfile()
    } 

    handleFetchProfile(){
        this.setState({showloader: true}, () => {
            this.props.dispatch(fetchProfile()).then(() => {
                if (this.props.profile.data.fetched && this.props.profile.data.result && this.props.profile.data.result.data && this.props.profile.data.result.data.length > 0) {
                    this.setState({
                        _id: this.props.profile.data.result.data[0]._id,
                        fullName: this.props.profile.data.result.data[0].fullName,
                        age: this.props.profile.data.result.data[0].age,
                        sex: this.props.profile.data.result.data[0].sex,
                        email: this.props.profile.data.result.data[0].email,
                        showLoader: false,
                        checkFiles: true,
                        changedData: false
                    })
                } else {
                    this.setState({
                        showLoader: false
                    })
                }
            });
        })
    }

    handleValuesChange(e) { 
		this.setState({
		    [e.target.name]: e.target.value,
		    changedData: e.target.value != this.props.profile.data.result.data[0][[e.target.name]]
		});
	}

    formValidation = (error, name) => {
        this.setState({ [name + "Error"]: error}, () => {
            this.setState({ 
                showFormErrors: this.state.emailError || this.state.fullNameError || this.state.sexError || this.state.ageError
             });  
        });
    }

    checkData(){
        const newDt = {};
        let aux = 0;
        Object.entries(this.props.profile.data.result.data[0]).map((item) => {
            if ( item[1] != this.state[item[0]] ) {
                newDt[item[0]] = this.state[item[0]]
                aux ++
            }
        })
        this.setState({newData: newDt})        
        return aux
    }


    handleProfileUpdate(e){
        e.preventDefault();
        if (!this.state.showFormErrors && this.checkData()) {
            this.setState({ showLoader: true }, () => {
                const data = Object.assign({}, this.state.newData)
                this.props.dispatch(updateProfile(this.state._id, data)).then(()=>{
                    if(this.props.profile.update.fetched){
                        this.handleFetchProfile()
                    }else{
                        this.setState({ showLoader: false })
                    }
                })
            })
        }
    }

    render() {
        return (
            <Page pageClass={'profile'}>
                <Helmet>
                    <meta name="description" content="Profile" />
                </Helmet>
                <Header />
                <Breadcrumbs items={[{ title: 'Home', link: "/", position: 1 },{ title: 'Profile', link: "/profile", position: 2 }]} />
                <main className='main'>
                    <div className="container">              
                        <Sidebar />
                        <Section sectionClass="profile" title="Profile">
                            <Form>
                               <div className="form__row">
                                    <Input type={'text'} checkFiles={this.state.checkFiles} showErrors={this.state.showFormErrors} formValidation={this.formValidation} name={'fullName'} label={'Full Name*'} className={'fullname'} onChange={this.handleValuesChange} value={this.state.fullName} required={{ minChars: 1 }} />
                                </div>
                                <div className="form__row">
                                    <Input type={'text'} checkFiles={this.state.checkFiles} showErrors={this.state.showFormErrors} formValidation={this.formValidation} name={'email'} label={'Email*'} className={'email'} onChange={this.handleValuesChange} value={this.state.email} regex={'email'} required={{ minChars: 3, maxChars: 150 }} />
                                    <Input type={'number'} checkFiles={this.state.checkFiles} showErrors={this.state.showFormErrors} formValidation={this.formValidation} name={'age'} label={'Age*'} className={'age'} onChange={this.handleValuesChange} value={this.state.age} required={{ minChars: 1, maxChars: 3 }} />
                                    <Select checkFiles={this.state.checkFiles} showErrors={this.state.showFormErrors} formValidation={this.formValidation} name={'sex'} label={'Sex*'} className={'sex'} onChange={this.handleValuesChange} selectedOption={this.state.sex} options={this.state.sexOptions} placeholder={'Choose one'} />
                                </div>
                                <div className="form__row">
                                    <div className="form__cta">
                                        <button className="button button--black" onClick={this.handleProfileUpdate} disabled={this.state.showFormErrors || !this.state.changedData}>Salvar alterações</button>
                                    </div>
                                </div>
                            </Form>
                        </Section>
                   </div>
                </main>
                <Loader show={this.state.showLoader} > {this.state.loaderMessages} </Loader>
                <Footer />
            </Page>
        );
    }
}

export default connect((store) => {
    return {
        profile: store.profileState
    };
})(withRouter(Profile));
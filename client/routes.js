import Home from "./container/Home";
import AddressList from "./container/AddressList";
import OrderHistory from "./container/OrderHistory";
import Profile from "./container/Profile";
import Wishlist from "./container/Wishlist";

export default [
	{
		path: '/',
		exact: true,
		component: Home,
	},	
	{
		path: '/address-list',
		exact: true,
		component: AddressList,
	},
	{
		path: '/order-history',
		exact: true,
		component: OrderHistory,
	},
	{
		path: '/profile',
		exact: true,
		component: Profile,
	},
	{
		path: '/wishlist',
		exact: true,
		component: Wishlist,
	}
];

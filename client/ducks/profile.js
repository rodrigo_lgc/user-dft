import axios from 'axios';
import Immutable from 'immutee';

import { config } from './../../config/config';

export const FETCH_USER_PROFILE = 'user/FETCH_USER_PROFILE';
export const FETCH_USER_PROFILE_FAILURE = 'user/FETCH_USER_PROFILE_FAILURE';
export const UPDATE_USER_PROFILE = 'user/UPDATE_USER_PROFILE';
export const UPDATE_USER_PROFILE_FAILURE = 'user/UPDATE_USER_PROFILE_FAILURE';

const initialState = {
	data: {
		fetched: false,
		result: [],
		error: null
	},
	update: {
		fetched: false,
		result: [],
		error: null
	}
};

export default function reducer(state = initialState, action) {
	const immutable = Immutable(state);
	switch (action.type) {
		case FETCH_USER_PROFILE:
			return immutable
				.set('data.result', action.payload)
				.set('data.fetched', true)
				.set('data.error', null)
				.done();

		case FETCH_USER_PROFILE_FAILURE:
			return immutable
				.set('data.error', action.payload)
				.set('data.result', [])
				.set('data.fetched', false)
				.done();

		case UPDATE_USER_PROFILE:
			return immutable
				.set('update.result', action.payload)
				.set('update.fetched', true)
				.set('update.error', null)
				.done();

		case UPDATE_USER_PROFILE:
			return immutable
				.set('update.error', action.payload)
				.set('update.result', [])
				.set('update.fetched', false)
				.done();
		default:
			return state;
	}
}

export function fetchProfile() {
	return (dispatch) => {
		return axios.get('/user/profile')
			.then((res) => {
				dispatch({
					type: FETCH_USER_PROFILE,
					payload: res.data
				});			
			}).catch((err) => {
				dispatch({
					type: FETCH_USER_PROFILE_FAILURE,
					payload: err.stack
				});
			});
	}
}

export function updateProfile(id,data) {
	return (dispatch) => {
		return axios.put('/user/profile/' + id, data)
			.then((res) => {			
				dispatch({
					type: UPDATE_USER_PROFILE,
					payload: res.data
				});			
			}).catch((err) => {
				dispatch({
					type: UPDATE_USER_PROFILE_FAILURE,
					payload: err.stack
				});
			});
	}
}
import React, { Component } from 'react';

import { config } from './../../../config/config';

export default class Footer extends Component {
	render() {
		return (
			<footer className="footer">
				<div className="container">
					<span className="footer__copyright">© Copyright.</span>
				</div>
			</footer>
		);
	};
};
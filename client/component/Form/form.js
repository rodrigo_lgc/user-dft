import React, { Component } from 'react';

export default class Form extends Component {
	render() {
		return (
			<form className={'form' + (this.props.formClass ? ' '+this.props.formClas: '')}>
                {this.props.children}
			</form>
		);
	}
}
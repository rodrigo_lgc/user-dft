import React, { Component } from 'react';
export default class Input extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: this.props.value,
			inError: false,
			errorMessage: undefined,
			validation: {
				rules: {
					email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
					surname: /^[A-Z][a-z]*(-|\s)[A-Z][a-z]*$/
				},
				messages: {
					email: {
						regex: 'Invalid email',
						required: 'Email field should be filled and it must have at least {#chars} characters.'
					},
					surname: {
						regex: 'Name field must have surname',
						required: 'Name field should be filled and it must have at least {#chars} characters.'
					}
				}
			}
		}
		this.validateField = this.validateField.bind(this);
	}

	componentDidUpdate(prevProps, prevState, snapshot){
		if (this.props.checkFiles && !prevProps.checkFiles ){
			this.validateField()
		}
	}
	
	showErrorMessage() {
		let _errorMessage = undefined;
		if (this.props.errorMessage) {
			_errorMessage = this.props.errorMessage;
		} else {
			_errorMessage = 'Invalid Field';
		}
		this.setState({errorMessage: _errorMessage})
	}

	validateRequiredField() {
		let _errorRequired = false;
		if (typeof this.props.required === 'object') {
			let _minChars = (this.props.required.minChars) ? this.props.required.minChars : false;
			let _maxChars = (this.props.required.maxChars) ? this.props.required.maxChars : false;

			if (_minChars && this.state.value.length < _minChars || _maxChars && this.state.value.length > _maxChars) {
				_errorRequired = true;
			}

		} else {
			if (this.state.value) {
				_errorRequired = false;
			} else {
				_errorRequired = true;
			}
		}
		return _errorRequired;
	}
	
	validateField() {
		let _inError = this.validateRequiredField();
		if (!_inError && this.props.regex) {
			if (this.state.validation.rules[this.props.regex]) {
				_inError = !this.state.validation.rules[this.props.regex].test(this.state.value);
			} else {
				_inError = !this.props.regex.test(this.state.value);
			}
		}
		this.props.formValidation(_inError,this.props.name)
		this.setState({inError: _inError}, () => this.showErrorMessage());
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.value != this.props.value) {
			this.setState({value: (this.props.type == 'email') ? nextProps.value.toLowerCase() : nextProps.value});
		}
		if (nextProps.showErrors && nextProps.showErrors != this.props.showErrors){
			this.validateField()			
		}
	}

	renderInputCheckbox() {
		return (
			<div className={'form__field form__field--checkbox' + (this.props.className ? ' ' + this.props.className : '')} data-name={this.props.name} data-checked={(this.props.checked) ? true : false} onClick={(e) => this.props.onChange(e)}>
				<label className="form__field__label">
					<span className="form__field__checkbox">&nbsp;</span>
					{this.props.label}
				</label>
			</div>
		)
	}
	renderInputText() {
		return (
			<div className={'form__field form__field--text' + (this.props.className ? ' ' + this.props.className : '') + (this.state.inError ? ' form__field--error' : '')}>
				<input type={(this.props.type) ? this.props.type : 'text'} name={this.props.name} className={(this.props.value) ? 'form__field__filled' : null} onChange={(e) => this.props.onChange(e)} value={this.state.value} onBlur={this.validateField} autoComplete="off" />
				<span className="form__field__bar">&nbsp;</span>
				<label className="form__field__label">{this.props.label}</label>
				{this.state.inError ? <span className="form__field__error"> {this.state.errorMessage} </span> : ''}
			</div>
		)
	}
	render() {

		if (this.props.type == 'checkbox') {
			return this.renderInputCheckbox();
		} 

		return this.renderInputText()
	}
}
import React, { Component } from 'react';

export default class Select extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value,
            inError: false,
            errorMessage: undefined,
        }
        this.validateField = this.validateField.bind(this);
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.selectedOption != this.props.selectedOption) {
            
            this.setState({ value: nextProps.selectedOption });
        }
        if (nextProps.showErrors && nextProps.showErrors != this.props.showErrors) {
            this.validateField()
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.checkFiles && !prevProps.checkFiles) {
            this.validateField()
        }
    }

    showErrorMessage() {
        let _errorMessage = undefined;
        if (this.props.errorMessage) {
            _errorMessage = this.props.errorMessage;
        } else {
            _errorMessage = 'Choose one option';
        }
        this.setState({
            errorMessage: _errorMessage
        })
    }

    validateRequiredField() {
        let _errorRequired = false;

        if (this.state.value) {
            _errorRequired = false;
        } else {
            _errorRequired = true;
        }

        return _errorRequired;
    }

    validateField() {
        let _inError = this.validateRequiredField();
        
        this.props.formValidation(_inError, this.props.name)
        this.setState({ inError: _inError }, () => this.showErrorMessage());
    }

    render(){

        return (
            <div className={"form__field form__field--select" + (this.props.className ? ' ' + this.props.className : '') + (this.state.inError ? ' form__field--error' : '')}>
                <label className="form__field__label">{this.props.label}</label>
                <select name={this.props.name} value={this.props.selectedOption} onChange={(e) => this.props.onChange(e)} value={this.state.value} onBlur={this.validateField} >
                    {this.props.placeholder ? <option value="">{this.props.placeholder}</option> : null}
                    {
                        this.props.options
                            ? this.props.options.map((opt,i) => { return (<option key={i} value={this.props.optionKey ? opt[this.props.optionKey] : opt}>{this.props.optionValue ? opt[this.props.optionValue] : opt}</option>) })
                            : null
                    }
                </select>
                {this.props.error ? <span className="form__field__error">{this.state.errorMessage}</span> : null}
            </div>
        )
    }
	
};
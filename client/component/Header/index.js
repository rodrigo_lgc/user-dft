import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { Link } from 'react-router-dom'

class Header extends Component {

	render() {		
		
		return (
			<header className="header">
				<div className="container">					
					<h1 className="header__brand">
						<Link className={'header__brand__link'} to={'/'} title="Home">Home</Link>
					</h1>
				</div>
			</header>
		);
	};
};

export default withRouter(Header)
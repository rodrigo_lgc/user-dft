import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class Sidebar extends Component {
	render() {
		return (
            <aside className='sidebar'>
                <nav>
                    <ul className='sidebar__list'>
                        <li className='sidebar__list__item'>
                            <NavLink to="/" className='sidebar__list__item__link' exact activeClassName="sidebar__list__item__link--active">Home</NavLink>
                        </li>
                        <li className='sidebar__list__item'>
                            <NavLink to="/profile" className='sidebar__list__item__link' activeClassName="sidebar__list__item__link--active">Profile</NavLink>
                        </li>
                        <li className='sidebar__list__item'>
                            <NavLink to="/address-list" className='sidebar__list__item__link' activeClassName="sidebar__list__item__link--active">Address List</NavLink>
                        </li>
                        <li className='sidebar__list__item'>
                            <NavLink to="/order-history" className='sidebar__list__item__link' activeClassName="sidebar__list__item__link--active">Order History</NavLink>
                        </li>
                        <li className='sidebar__list__item'>
                            <NavLink to="/wishlist" className='sidebar__list__item__link' activeClassName="sidebar__list__item__link--active">Whishlist</NavLink> 
                        </li>                    
                    </ul>
                </nav>
            </aside>
		);
	};
};
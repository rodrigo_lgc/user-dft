import React, { Component } from 'react';

export default class Breadcrumbs extends Component {
	constructor(props) {
		super(props);
		this.state = {
			items: this.props.items,
			itemsClass: this.props.itemsClass,
		}
	}

	getItems() {
		let _items = [];
		const items = this.state.items.map((item, index) => {
			let crumbs = <li key={index} itemProp="itemListElement" itemScope="" itemType="http://schema.org/ListItem" className={(this.state.itemsClass) ? 'breadcrumbs__item ' + this.state.itemsClass : 'breadcrumbs__item'}>
							<a itemProp="item" href={item.link}>
								<span itemProp="name">{item.title}</span>
							</a>
							<meta itemProp="position" content={item.position} />
						</li>
			if (index == this.state.items.length - 1){
				crumbs = <li key={index} itemProp="itemListElement" itemScope="" itemType="http://schema.org/ListItem" className={(this.state.itemsClass) ? 'breadcrumbs__item ' + this.state.itemsClass : 'breadcrumbs__item'}>
							<span itemProp="name">{item.title}</span>
							<meta itemProp="url" content={item.link} />
							<meta itemProp="position" content={item.position} />
						</li>
			}
			_items.push(crumbs)
		});
		return _items;
	
	}
	render() {
		return (
			<div class='breadcrumbs'>
				<div className="container">
					<ol itemScope itemType="http://schema.org/BreadcrumbList">
						{this.getItems()}
					</ol>
				</div>
			</div>
		);
	}
}